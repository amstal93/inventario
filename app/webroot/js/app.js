
//    sidebar dropdown menu auto scrolling

jQuery('#sidebar .sub-menu > a').click(function () {
  var o = ($(this).offset());
  diff = 250 - o.top;
  if(diff>0)
      $("#sidebar").scrollTo("-="+Math.abs(diff),500);
  else
      $("#sidebar").scrollTo("+="+Math.abs(diff),500);
});

jQuery('.toggleLink').click(function(e) {
  e.preventDefault();
  $(e.target).siblings('.toggleTarget').slideToggle();
});


//    sidebar toggle

$(function() {
  function responsiveView() {
      var wSize = $(window).width();
      if (wSize <= 768) {
          $('#container').addClass('sidebar-close');
          $('#sidebar > ul').hide();
      }

      if (wSize > 768) {
          $('#container').removeClass('sidebar-close');
          $('#sidebar > ul').show();
      }
  }
  $(window).on('load', responsiveView);
  $(window).on('resize', responsiveView);
});

$('.fa-bars').click(function () {
  if ($('#sidebar > ul').is(":visible") === true) {
      $('#main-content').css({
          'margin-left': '0px'
      });
      $('#sidebar').css({
          'margin-left': '-210px'
      });
      $('#sidebar > ul').hide();
      $("#container").addClass("sidebar-closed");
  } else {
      $('#main-content').css({
          'margin-left': '210px'
      });
      $('#sidebar > ul').show();
      $('#sidebar').css({
          'margin-left': '0'
      });
      $("#container").removeClass("sidebar-closed");
  }
});

/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'fast',
        showCount: false,
        autoExpand: true,
        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('select').formSelect();

    var updateContainerList = function (callback = null) {
        $('#container-id').val(0);
        $.post('/containers/getByRoom', {
            'room_id': $('#room-id').val(),
            '_csrfToken': $('input[name=_csrfToken]').val()
        }, function(response) {
            if (response.length) {
                var containerList = $("#container-id");
                containerList.empty();
                containerList.empty().append($('<option></option>').attr("value", "").text("None"))
                for (ii in response) {
                    var container = response[ii]
                    var option = $('<option></option>').attr("value", container.id).text(container.title);
                    containerList.append(option);
                }
                containerList.attr('disabled',false)
                containerList.formSelect();
                $('#container-title').val('')
                $('#containerSelectWrap').show();
                if (callback) {
                    callback()
                }
            } else {
                $('#containerSelectWrap').hide();
            }
        });
    }
    var updateRoomList = function (callback = null) {
        $.ajax({
            url:'/rooms/index',
            type:"POST",
            data:{},
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function(response) {
                console.log(response)
                if (response.length) {
                    console.log("Do it")
                    var roomList = $("#room-id");
                    roomList.empty();
                    roomList.empty().append($('<option></option>').attr("value", "").text("None"))
                    for (ii in response) {
                        var room = response[ii]
                        var option = $('<option></option>').attr("value", room.id).text(room.title);
                        roomList.append(option);
                    }
                    roomList.attr('disabled',false)
                    roomList.formSelect();
                    $('#room-title').val('')
                    if (callback) {
                        callback()
                    }
                }
            }
        });
    }
    var handleQuickAdd = function (response) {
        var tags = $('#set-tags').select2('data');
        console.log(tags)
        var tagString = "";
        for(tag in tags) {
            console.log(tags[tag].text);
            console.log(tags[tag]);
            tagString += tags[tag].text+", ";
        }
        console.log(tagString)
        $.post('/assets/add', {
            'title': $('#quick-add').val(),
            'room_id': $('#room-id').val(),
            'room_title': $('#room-title').val(),
            'container_id': $('#container-id').val(),
            'container_title': $('#container-title').val(),
            'quantity': $('#quantity').val(),
            'amount_paid': $('#amount-paid').val(),
            'tags': tagString,
            '_csrfToken': $('input[name=_csrfToken]').val()
        }, function(response) {
            console.log(response)
            if (typeof response === 'object') {
                if (response.new) {
                    var verb = "Added"
                    var alertClass = "alert-success"
                } else if (response.new == false) {
                    var verb = "Updated"
                    var alertClass = "alert-warning"
                }
                var image = "";
                if (response.image_url) {
                    image = "<img src='"+response.image_url+"' height=50 /> ";
                }
                var assetItem = "<div class='assetItem alert "+alertClass+"'>"+
                    "<a href='/assets/view/"+response.asset_id+"'>"+
                    image+verb+" "+response.title
                if (response.price) {
                    " - $"+response.price
                }
                if (response.container_title) {
                    assetItem += " - "+response.container_title
                }
                if (response.room_title) {
                    assetItem += " - "+response.room_title
                }
                assetItem += "</a></div>"
                $('#quick_results').prepend(assetItem)
                $('#quick-add').attr('disabled',false).val('')
                $('#amount-paid').val('')
                updateRoomList(function () {
                    if (response.room_id) {
                        $('#room-id').find('option[value="'+response.room_id+'"]').prop('selected', true);
                        $('#room-id').formSelect()
                    }
                    updateContainerList(function () {
                        if (response.container_id) {
                            $('#container-id').find('option[value="'+response.container_id+'"]').prop('selected', true);
                            $('#container-id').formSelect()
                        }
                    })
                })
            } else {
                var assetItem = "<div class='assetItem alert alert-danger'>Invalid UPC or Server Error.</div>"
                $('#quick_results').prepend(assetItem)
                $('#quick-add').attr('disabled',false)
            }
        });
    }

    $('#quickAdd #room-id').change(function (event) {
        updateContainerList()
        $('#room-title').val('')
    });
    $('#quickAdd form').submit(function (event) {
        event.preventDefault()
        $('#quick-add').attr('disabled',true)
        handleQuickAdd(event);
    })
    $('#room-title').keyup(function(event) {
        $('#room-id').find('option[value=""]').prop('selected', true);
        $('#room-id').formSelect()
    })
    $('#container-title').keyup(function(event) {
        $('#container-id').find('option[value=""]').prop('selected', true);
        $('#container-id').formSelect()
    })
    $('#quick-add').keyup(function(event) {
        console.log("keyp up")
        var upc = $(event.target).val();
        console.log(upc.length,!isNaN(upc))
        if (upc.length>11 && !isNaN(upc)) {
            $('#quick-add').attr('disabled',true)
            handleQuickAdd(event);
        }
    });
    $("#set-tags").select2({
        tags: true,
        tokenSeparators: [',']
    })

    $('#fileUploadButton').click(function (event) {
        event.preventDefault();
        $('#photo').click()
    })

    $('#photo').change(function() {
        $('#fileUpload').submit();
    });
});