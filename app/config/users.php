<?php
return [
    'Users' => [
        'table' => 'MyUsers',
        'Email' => [
        // determines if the user should include email
        'required' => true,
        // determines if registration workflow includes email validation
        'validate' => false,
        ],
        'Registration' => [
            // determines if the register is enabled
            'active' => true,
            // determines if the reCaptcha is enabled for registration
            'reCaptcha' => false,
            //ensure user is active (confirmed email) to reset his password
            'ensureActive' => false,
            // default role name used in registration
            'defaultRole' => 'admin',
        ],
        'Tos' => [
            // determines if the user should include tos accepted
            'required' => false,
        ],
    ]
];