<?php
use Migrations\AbstractMigration;

class AddUsersToFiles extends AbstractMigration
{

    public function up()
    {

        $this->table('files')
            ->addColumn('user_id', 'uuid', [
                'after' => 'room_id',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('files')
            ->removeColumn('user_id')
            ->update();
    }
}

