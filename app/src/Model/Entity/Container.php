<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Container Entity
 *
 * @property string $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property string|null $title
 * @property string|null $description
 * @property int|null $room_id
 *
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\Asset[] $assets
 */
class Container extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'title' => true,
        'description' => true,
        'room_id' => true,
        'room' => true,
        'assets' => true
    ];
}
