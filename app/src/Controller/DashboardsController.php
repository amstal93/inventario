<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dashboards Controller
 */
class DashboardsController extends AppController
{

    public function overview()
    {
        $this->loadModel('Assets');
        $assets = $this->Assets->find('all',[
            'conditions' => [
                'user_id' => $this->request->getSession()->read('Auth.User.id')
            ]
        ]);
        $assetCount = $assets->count();

        $totalValue = 0;
        foreach ($assets as $asset) {
            $totalValue += $asset->found_price_median;
        }

        $this->loadModel('Rooms');
        $roomsCount = $this->Rooms->find('all',[
            'conditions' => [
                'user_id' => $this->request->getSession()->read('Auth.User.id')
            ]
        ])->count();

        $this->loadModel('Containers');
        $containersCount = $this->Containers->find('all',[
            'conditions' => [
                'user_id' => $this->request->getSession()->read('Auth.User.id')
            ]
        ])->count();

        $this->loadModel('Files');
        $filesCount = $this->Files->find('all',[
            'conditions' => [
                'user_id' => $this->request->getSession()->read('Auth.User.id')
            ]
        ])->count();

        $this->set(compact('totalValue','assetCount','roomsCount', 'containersCount', 'filesCount'));
    }
}
