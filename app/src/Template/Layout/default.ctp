<?php
/**
 * @var \App\View\AppView $this
 */
?>

<!doctype html>
<html>
    <head>
        <?php echo $this->Html->charset();
        $this->Html->script('header/notificationsPopout', ['block' => 'scriptBottom']); ?>
        <title>
        Inventario - <?= $this->fetch('title') ?>
        </title>
        <?php
        echo $this->Html->meta('icon'); 
        echo $this->AssetCompress->css('all');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->AssetCompress->script('headlibs');
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Ruda" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    <body<?php if (isset($bodyClass)){echo ' class="'.$bodyClass.'"';}?>>
        <section id="container"<?php if (isset($hideSidebar)) { ?> class='sidebar-closed'<?php } ?>>
            <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
            <!--header start-->
            <?php if (!isset($hideHeader)) { ?>
            <header class="header black-bg<?php if (env('DEV')) { echo " header-dev"; } ?>">
                <?php if (!isset($hideSidebar)) { ?>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right"></div>
                </div>
                <?php } ?>
                <!--logo start-->
                <a href="/" class="logo">
                    <img src='/img/logo.png' height='20' style='margin-top: -5px; margin-right: .5em;'/><b>Inventario</b>
                </a>
                <!--logo end-->
                
                <?php //echo $this->cell('Notifications::popout'); ?>

                <div class="top-menu">
                    <ul class="nav pull-right top-menu hidden-xs">
                        <li><a data-toggle="dropdown" class="dropdown-toggle">
                        <i class="fa fa-life-saver"></i>
                        </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href='https://gitlab.com/NickBusey/inventario' target='_new'><i class="fa fa-ticket"></i> GitLab</a></li>

                            </ul>
                        </li>
                        <li>
                        <?php if ($this->request->getSession()->read('Auth.User.id')) { ?>
                        <a class='logout' href='/users/users/logout'>Logout</a>
                        <?php } else {?>
                        <a class='login' href='/users/users/login'>Login</a>
                        <?php }?>
                        </li>
                    </ul>
                </div>

            </header>
            <?php } ?>
            <!--header end-->

            <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <?php if (!isset($hideSidebar)) { ?>
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <ul class="sidebar-menu" id="nav-accordion">
                        <p class="centered"><a href="/users/users/profile/<?=$this->request->getSession()->read('Auth.User.id')?>"><img src="https://www.gravatar.com/avatar/<?php echo md5( strtolower( trim( $this->request->getSession()->read('Auth.User.email') ) ) ); ?>?d=identicon" class="img-circle" width="60" height="60"></a></p>
                        <h5 class="centered"><a href="/users/users/profile/<?=$this->request->getSession()->read('Auth.User.id')?>" id="accountLink" data-userid='<?=$this->request->getSession()->read('Auth.User.id')?>'><?=$this->request->getSession()->read('Auth.User.username')?></a></h5>
                        <li class="mt">
                            <a href="/"<?php if ($this->request->getParam('controller')=='Dash'&&$this->request->getParam('action')=='index'){?> class='active'<?php }?>>
                                <i class="fa fa-home"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="/assets"<?php if ($this->request->getParam('controller')=='Assets'&&$this->request->getParam('action')=='index'){?> class='active'<?php }?>>
                                <i class="fa fa-box"></i>
                                <span>Assets</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="/assets/add"<?php if ($this->request->getParam('controller')=='Assets'&&$this->request->getParam('action')=='add'){?> class='active'<?php }?>>
                                <i class="fa fa-plus-circle"></i>
                                <span>Add Items</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="/containers"<?php if ($this->request->getParam('controller')=='Containers'){?> class='active'<?php }?>>
                                <i class="fa fa-boxes"></i>
                                <span>Containers</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="/rooms"<?php if ($this->request->getParam('controller')=='Rooms'){?> class='active'<?php }?>>
                                <i class="fa fa-door-open"></i>
                                <span>Rooms</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </aside>
            <?php } ?>
            <!--sidebar end-->

            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                <div id='flashMessages'>
                    <?= $this->Flash->render() ?>
                    <?= $this->Flash->render('auth', [
                        'element' => 'auth_custom'
                    ]); ?>
                </div>
                    <?php echo $this->fetch('content'); ?>
                </section>
            </section>
            <?php if (!isset($hideFooter)) { ?>
            <footer class="site-footer">
                <div class="text-center">
                    Inventario <?=env('VERSION')?> - by <a href="http://nickbusey.com/">Nick Busey</a>
                    <a href="#" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
            </footer>
            <?php } ?>
            <!--footer end-->
        </section>
        <?php 
        echo $this->AssetCompress->script('libs');
        echo $this->fetch('scriptBottom');
        ?>
        <script>
            jQuery(document).ready(function() {
              jQuery("abbr.timeago").timeago();
            });
        </script>
    </body>
</html>