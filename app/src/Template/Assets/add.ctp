<h3><i class="fa fa-angle-right"></i> Quick Add Items</h3>

<div class="col-lg-12" id="quickAdd">
    <div class="form-panel">
        <?= $this->Form->create($asset,['class'=>'form-horizontal style-form']) ?>
            <div class="form-group">
                <label class="control-label col-md-3">Room</label>
                <div class="col-md-3 col-xs-11">
                  <?=$this->Form->control('room_id', ['options' => $rooms, 'empty' => '- Select -', 'label'=>false]);?>
                  <p> - or - </p>
                  <?=$this->Form->control('room_title', ['label'=>'New Room Title']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Container</label>
                <div class="col-md-3 col-xs-11">
                    <div id='containerSelectWrap'>
                        <?=$this->Form->control('container_id', ['options' => [], 'empty' => '- Select a Room -', 'label'=>false, 'disabled'=>'disabled']);?>
                        <p> - or - </p>
                    </div>
                  <?=$this->Form->control('container_title', ['label'=>'New Container Title']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Amount Paid</label>
                <div class="col-md-3 col-xs-11">
                  <?=$this->Form->control('amount_paid', ['label'=>false]);?>
                  <span class="help-block">Optional.</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Quantity</label>
                <div class="col-md-3 col-xs-11">
                    <?=$this->Form->control('quantity',['default'=>1, 'label'=>false]);?>
                    <span class="help-block">Optional.</span>
                </div>
            </div>
            <div class="form-group tagsWrap">
                <label class="control-label col-md-3">Tags</label>
                <div class="col-md-3 col-xs-11">
                    <?php $tags = [];
                    foreach ($tagged as $tag) {
                        // debug($tag);
                        // $tagString .= $tag->tag->label.", ";
                        array_push($tags, $tag->tag->label);
                    }
                    ?>
                    <?=$this->Form->control('set_tags',['options'=>$tags,'multiple'=>'multiple','value'=>'Imported, ']);?>

                    <span class="help-block">Optional.</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Title or UPC</label>
                <div class="col-md-3 col-xs-11">
                  <?=$this->Form->control('quick_add',['label'=>false]);?>
                  <span class="help-block">Either enter a Title of the item being added, or enter a UPC. It auto submits as soon as it's filled with a valid UPC. Use a phone with a bardcode scanner keyboard for easy entry.</span>
                </div>
                <div id='quick_results' class='col-md-3 col-xs-11'></div>
            </div>
            <?= $this->Form->button(__('Submit',['class'=>'btn btn-submit'])) ?>
            <?= $this->Form->end() ?>
        </form>
    </div><!-- /form-panel -->
</div>