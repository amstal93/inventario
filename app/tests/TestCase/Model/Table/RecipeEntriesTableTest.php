<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecipeEntriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecipeEntriesTable Test Case
 */
class RecipeEntriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecipeEntriesTable
     */
    public $RecipeEntries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.recipe_entries',
        'app.recipes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RecipeEntries') ? [] : ['className' => RecipeEntriesTable::class];
        $this->RecipeEntries = TableRegistry::getTableLocator()->get('RecipeEntries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RecipeEntries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
