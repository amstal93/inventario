<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BatchEntriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BatchEntriesTable Test Case
 */
class BatchEntriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BatchEntriesTable
     */
    public $BatchEntries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.batch_entries',
        'app.recipe_entries',
        'app.batches'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BatchEntries') ? [] : ['className' => BatchEntriesTable::class];
        $this->BatchEntries = TableRegistry::getTableLocator()->get('BatchEntries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BatchEntries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
