# Inventario

Home Inventory System

## Development Status

Still very early days (project began 2018-11-25), but should be fairly usable. CakePHP migrations will be used
for any database changes, so there should be no backwards compatibility issues with upgrades.

## Features

* Automated UPC lookup and product import. ([Demo video](https://imgur.com/a/jQuoc6L))
* Add images of rooms, containers, and assets.
* Upload Manuals, Warranties and Receipts

![](screenshot.png)

## Installation

### HomelabOS

Inventario comes bundled with [HomelabOS](https://gitlab.com/NickBusey/HomelabOS). Just set `enable_inventario: True`
and you're good to go.

### Docker

Executing `./bootstrap.sh` should get you running. If you're just messing around locally open
[http://localhost:8080/](http://localhost:8080/). Warning! This is not at all ready to be used
in a public environment. User registration is open to anyone, and any new users are admin
by default.

## Support

Stop by #inventario on irc.freenode.net